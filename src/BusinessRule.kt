package dk.bank

import dk.parser.LoanRequest

object BusinessRule {
    fun getInterestRate(request: LoanRequest): Double {
        if (request.loanDuration <= 0) {
            return -1.0
        }

        val maxCreditScore = 800 / request.creditScore
        val days: Int = request.loanDuration
        return calculateInterestRate(maxCreditScore, days)
    }

    private fun calculateInterestRate(creditScore: Int, loanDuration: Int): Double =
            if (loanDuration > 365 * 20) {
                3.5 + creditScore
            } else {
                -0.15 * (loanDuration / 365) + 6.5 + creditScore
            }
}