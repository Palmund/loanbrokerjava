package dk.parser

import org.codehaus.jackson.annotate.JsonProperty

public class LoanRequest(
        JsonProperty("ssn") var ssn: String,
        JsonProperty("creditScore") var creditScore: Int,
        JsonProperty("loanAmount") var loanAmount: Int,
        JsonProperty("loanDuration") var loanDuration: Int
)
