package dk.parser

import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.type.TypeReference

/**
 * Created by Søren Palmund on 10/12/14.
 */
public class Parser {
    val mapper = ObjectMapper()

    fun toJson(input: Any): String = mapper.writeValueAsString(input)

    fun <T> fromJson(input: String, type: TypeReference<T>): T = mapper.readValue<T>(input, type)
}