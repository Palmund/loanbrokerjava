package dk.parser

import org.codehaus.jackson.annotate.JsonProperty

public class LoanResponse(
        JsonProperty("ssn") val ssn: String,
        JsonProperty("interestRate") val interestRate: Double
)
