import dk.parser.Parser
import dk.parser.LoanRequest
import dk.bank.BankClient.LoanRequestType
fun main(args: Array<String>) {
    val inJson = "{\"ssn\":\"2004901897\"}"
    val j = Parser().fromJson<LoanRequest>(inJson, LoanRequestType())
    println(j.ssn)
    println(j.creditScore)
    println(j.loanAmount)
    println(j.loanDuration)

    println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~")

    val loanRequest = LoanRequest("2004901897", 2000, 2000, 5)
    val json = Parser().toJson(loanRequest)
    println(json)

    //    val client = BankClient("MsgBankJava", ".\\Private$\\JsonNormalizerQueue")
    //    client.beginReceive()
}