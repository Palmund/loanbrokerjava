package dk.bank

import kotlin.properties.Delegates
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.QueueingConsumer
import com.rabbitmq.client.Connection
import com.rabbitmq.client.AMQP
import dk.parser.Parser
import dk.parser.LoanRequest
import dk.parser.LoanResponse
import org.codehaus.jackson.type.TypeReference

class BankClient(val receiveFrom: String, val sendTo: String, val exchange: String = "") {
    private val connection: Connection by Delegates.lazy {
        val factory = ConnectionFactory()
        factory.setHost("datdb.cphbusiness.dk")
        factory.setUsername("ivan")
        factory.setPassword("cph")
        factory.newConnection()
    }

    private val parser = Parser()

    fun beginReceive() {
        val channel = connection.createChannel()
        val consumer = QueueingConsumer(channel)

        // Ensure the queue exists...
        channel.queueDeclare(receiveFrom, false, false, true, mapOf())
        // Then let's receive messages!
        channel.basicConsume(receiveFrom, true, consumer)

        println("Waiting for messages...")

        while (true) {
            val delivery = consumer.nextDelivery()
            val bytes = String(delivery.getBody())

            val request = parser.fromJson(bytes, LoanRequestType())

            println("Received: '" + request + "'")

            val interestRate = BusinessRule.getInterestRate(request)
            val response = LoanResponse(request.ssn, interestRate)
            val packedResponse = parser.toJson(response)

            send(packedResponse.toByteArray(), delivery.getProperties())
        }
    }

    fun send(message: ByteArray, properties: AMQP.BasicProperties) {
        val channel = connection.createChannel()
        channel.basicPublish(exchange, sendTo, properties, message)
    }

    class LoanRequestType : TypeReference<LoanRequest>() {
    }
}